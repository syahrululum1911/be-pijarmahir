'use strict';

const MySQL = require('../../../../helpers/databases/mariadb/db');
const Mongo = require('../../../../helpers/databases/mongodb/db');
const wrapper = require('../../../../helpers/utils/wrapper');
const config = require('../../../../infra/configs/global_config');

const insertOneUser = async (document) => {
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('user');
    const result = await db.insertOne(document);
    return result;
}
const updateOneUser = async (params, document) => {
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('user');
    //const id = params.id
    const recordset = await db.upsertOne(params,document);
    return recordset;
}

module.exports = {
    insertOneUser: insertOneUser,
    updateOneUser: updateOneUser
}