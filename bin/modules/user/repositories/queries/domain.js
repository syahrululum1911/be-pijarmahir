'use strict';

const nconf = require('nconf');
const rp = require('request-promise');
const model = require('./query_model');
const query = require('./query');
const wrapper = require('../../../../helpers/utils/wrapper');
const config = require('../../../../infra/configs/global_config');
const validate = require('validate.js');
const logger = require("../../../../helpers/utils/logger");
class User{

    async viewAllUser(){
        const user = await query.findAllUser();
        const { data } = user

        if(user.err){
            return wrapper.error('error', 'Can not find user!', 404);
        }

        return wrapper.data(data, '', 200);
    }

    async viewOneUser(userId){
        const user = await query.findOneUser(userId);
        const { data } = user

        if(user.err){
            return wrapper.error('error', 'Can not find user!', 404);
        }
        
        return wrapper.data(data, '', 200);
    }

    async viewCountUser(param){
        const user = await query.countData(param);
        const { data } = user

        if(user.err){
            return wrapper.error('error', 'Can not find user!', 404);
        }
        
        return wrapper.data(data, '', 200);
    }
}

module.exports = User;