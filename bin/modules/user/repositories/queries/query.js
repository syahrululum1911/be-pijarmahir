'use strict';

const Mongo = require('../../../../helpers/databases/mongodb/db');
const MySQL = require('../../../../helpers/databases/mysql/db');
const wrapper = require('../../../../helpers/utils/wrapper');
const config = require('../../../../infra/configs/global_config');
const ObjectId = require('mongodb').ObjectId;

const findAllUser = async (parameter) => {
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('user');
    const recordset = await db.findAllData(['_id', 'username', 'password'], 10, 1);
    return recordset;
}

const findOneUser = async (parameter) => {
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('user');
    const recordset = await db.findOne(parameter);
    return recordset;
}

const findById = async (id) => {
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('user');
    const parameter = {
        _id: ObjectId(id)
    }
    const recordset = await db.findOne(parameter);
    return recordset;
}
const countData = async (parameter) => {
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('user');
    const recordset = await db.countData(parameter);
    return recordset;
}

module.exports = {
    findAllUser,
    findOneUser,
    findById,
    countData
}